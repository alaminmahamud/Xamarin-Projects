[Activity(Label="MainActivity", MainLauncher=true, Icon = "@drawable/icon")]
public class MainActivity:Activity
{
	protected void OnCreate(Bundle bundle)
	{

		base.OnCreate(bundle);

		/// Set our view from the "main" Layout Resource
		SetContentView(Resource.Layout.Main);
		/// Additional Setup COde will go here
	}

}