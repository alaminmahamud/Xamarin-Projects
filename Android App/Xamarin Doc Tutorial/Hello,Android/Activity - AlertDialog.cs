[Activity(Label="MainActivity", MainLauncher=true, Icon = "@drawable/icon")]
public class MainActivity:Activity
{
	protected void OnCreate(Bundle bundle)
	{

		base.OnCreate(bundle);

		/// Set our view from the "main" Layout Resource
		SetContentView(Resource.Layout.Main);
		/// Additional Setup COde will go here

		/// Controls LookUp

		EditText editText = FindViewById<EditText>(Resource.Id.PhoneNumbereText);
		EditText translateButton = FindViewById<EditText>(Resource.Id.PhoneNumbereText);
		Button callButton = FindViewById<Button>(Resource.Id.PhoneNumbereText);


		/// responding to User Interaction
		translateButton.Click += (object sender, EventArgs e) =>
		{
			// Translate user's alphaneumeric phone number to numeric
			translatedNumber = Core.PhoneWordTranslator.ToNumber(PhoneNumbereText.Text);
			if(String.IsNullOrWhiteSpace(translatedNumber))
			{
				callButton.Text = "Call";
				callButton.Enabled = false;
			}
			else
			{
				callButton.Text = "Call " + translatedNumber;
				callButton.Enabled = true;
			}
		}

		/// Display An alert Dialog
		var CallDialog = new AlertDialog.Builder(this);
		callDialog.SetMessage("Call", + translatedNumber + "?");
		callDialog.SetNeutralButton("Call", delegate
			{
				// Create Intent To Dial Phone
			});
		callDialog.SetNegativeButton("Cancel", delegate{});

		// Show the AlertDialog To the User and Wait For Response
		callDialog.Show();

	}

}